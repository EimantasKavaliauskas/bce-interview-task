import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {Message} from '../../../../interfaces/message';
import {MessageComment} from '../../../../interfaces/message-comment';
import {ConnectorService} from '../../../../services/connector.service';
import {Guid} from '../../../../services/guid-generator';
import {any} from 'codelyzer/util/function';
import {CommunicationIndicatorService} from '../../../../services/communication-indicator.service';
import {ValidatorService} from '../../../../services/validator.service';
import {MessageEventMediatorService} from '../../../../services/message-event-mediator.service';

@Component({
  selector: 'app-message-edit-dialog',
  templateUrl: './message-edit-dialog.component.html',
  styleUrls: ['./message-edit-dialog.component.scss']
})
export class MessageEditDialogComponent implements OnInit {

  message: Message;
  newComment: MessageComment = {id: Guid.newGuid(), email: '', text: '', addedOn: null, messageId: Guid.newGuid()};

  constructor(public dialogRef: MatDialogRef<MessageEditDialogComponent>,
              private messageEventMediator: MessageEventMediatorService,
              private connector: ConnectorService,
              private communicationIndicator: CommunicationIndicatorService,
              private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.message = data.message;
  }

  ngOnInit() {
    this.messageEventMediator.deleteMessageFromHub.subscribe(id => {
      if (this.message.id === id) {
        this.dialogRef.close();
      }
    });
  }

  validate(): boolean {
    if (this.newComment.email === '' || this.newComment.text === '') {
      this.showError('Please fill in all the fields', '');
      return false;
    }
    if (ValidatorService.validateEmail(this.newComment.email)) {
      return true;
    }
    this.showError('You have entered an invalid email address!', '');
    return false;
  }

  showError(message, action) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  onPostCommentClick() {
    if (!this.validate()) {
      return;
    }
    this.newComment.addedOn = new Date();
    this.newComment.messageId = this.message.id;
    this.message.comments.push(this.newComment);
    this.communicationIndicator.increaseSaveQueue();
    this.connector.PostComment(this.newComment).subscribe(result => {
      this.communicationIndicator.decreaseSaveQueue();
    });
    this.newComment = {id: Guid.newGuid(), email: '', text: '', addedOn: null, messageId: this.message.id};
  }

  onSaveClick() {
    this.communicationIndicator.increaseSaveQueue();
    this.connector.SaveMessage(this.message).subscribe(result => {
      this.communicationIndicator.decreaseSaveQueue();
    });
    this.dialogRef.close(true);
  }
}
