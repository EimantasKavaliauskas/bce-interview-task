import {Component, Input, OnInit} from '@angular/core';
import {MessageComment} from '../../../../../interfaces/message-comment';

@Component({
  selector: 'app-message-comment',
  templateUrl: './message-comment.component.html',
  styleUrls: ['./message-comment.component.scss']
})
export class MessageCommentComponent implements OnInit {

  @Input() comment: MessageComment;
  constructor() { }

  ngOnInit() {}

}
