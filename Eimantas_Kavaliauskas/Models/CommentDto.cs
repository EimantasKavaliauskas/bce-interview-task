﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eimantas_Kavaliauskas.Models
{
    public class CommentDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
        public string AddedOn { get; set; }
        public Guid MessageId { get; set; }
        
        public static CommentDto FromComment(Comment comment)
        {
            var commentDto = new CommentDto
            {
                AddedOn = comment.AddedOn,
                Email = comment.Email,
                Id = comment.Id,
                Text = comment.Text,
                MessageId = comment.MessageId,
            };
            return commentDto;
        }
    }
}
