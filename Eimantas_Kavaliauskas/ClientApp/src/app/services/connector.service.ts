import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Message} from '../interfaces/message';
import {MessageComment} from '../interfaces/message-comment';

@Injectable({
  providedIn: 'root'
})
export class ConnectorService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  public GetMessages(): Observable<Message[]> {
    return this.http.get<Message[]>( 'api/Messages');
  }

  public DeleteMessage(message: Message): Observable<Message> {
    return this.http.delete<Message>( 'api/Messages/' + message.id);
  }

  public SaveMessageList(messages: Message[]): Observable<Message[]> {
    return this.http.put<Message[]>('api/Messages/updateList', messages);
  }

  public SaveMessage(message: Message): Observable<Message> {
    return this.http.put<Message>('api/Messages/' + message.id, message);
  }

  public PostMessage(message: Message): Observable<Message> {
    return this.http.post<Message>('api/Messages/', message);
  }

  public PostComment(comment: MessageComment): Observable<MessageComment> {
    return this.http.post<MessageComment>('api/Comments/', comment);
  }
}
