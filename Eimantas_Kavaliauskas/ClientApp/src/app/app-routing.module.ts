import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MessageListComponent} from './components/message-list/message-list.component';

const routes: Routes = [
  { path: '', component: MessageListComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
