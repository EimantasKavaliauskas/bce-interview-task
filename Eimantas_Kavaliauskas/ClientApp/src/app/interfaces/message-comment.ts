export interface MessageComment {
  id: string;
  text: string;
  email: string;
  addedOn: Date;
  messageId: string;
}
