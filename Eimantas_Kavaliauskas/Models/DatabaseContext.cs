﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Eimantas_Kavaliauskas.Models
{
    public class DatabaseContext: DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
                : base(options)
        {
            
        }
        public DbSet<Message> Message { get; set; }
        public DbSet<Comment> Comment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .HasOne(p => p.Message)
                .WithMany(b => b.Comments);
        }
    }

    [Table("Message")]
    public class Message
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Email { get; set; }
        public int Index { get; set; }
        public List<Comment> Comments { get; set; }
        public string AddedOn { get; set; }
       
        public static Message FromMessageDto(MessageDto messageDto)
        {
            List<Comment> comments = new List<Comment>();
            messageDto.Comments.ForEach(comment => comments.Add(Comment.FromCommentDto(comment)));
            var message = new Message
            {
                Id = messageDto.Id,
                Index = messageDto.Index,
                Text = messageDto.Text,
                Title = messageDto.Title,
                AddedOn = messageDto.AddedOn,
                Comments = comments,
                Email = messageDto.Email
            };
            return message;
        }
    }

    [Table("Comment")]
    public class Comment
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
        public string AddedOn { get; set; }
        public Message Message { get; set; }
        public Guid MessageId { get; set; }

        public static Comment FromCommentDto(CommentDto commentDto)
        {
            var comment = new Comment
            {
                AddedOn = commentDto.AddedOn,
                Email = commentDto.Email,
                Id = commentDto.Id,
                MessageId = commentDto.MessageId,
                Text = commentDto.Text
            };
            return comment;
        }
    }
}
