import {MessageComment} from './message-comment';

export interface Message {
  id: string;
  title: string;
  text: string;
  email: string;
  addedOn: Date;
  index: number;
  comments: MessageComment[];
}
