﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eimantas_Kavaliauskas.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Eimantas_Kavaliauskas.Utilities
{
    public class StartupUtilities
    {
        private static bool _databaseChecked;

        public static async Task EnsureDatabaseCreated(IServiceProvider services)
        {
            if (!_databaseChecked)
            {
                var context = services.GetRequiredService<DatabaseContext>();

                _databaseChecked = true;
                //context.Database.Migrate();
                context.Database.Migrate();
            }
        }
    }
}
