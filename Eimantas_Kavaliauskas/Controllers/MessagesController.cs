﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Eimantas_Kavaliauskas.Models;
using Eimantas_Kavaliauskas.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace Eimantas_Kavaliauskas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public IHubContext<DataUpdateHub> _dataUpdateHub { get; }

        public MessagesController(DatabaseContext context, IHubContext<DataUpdateHub> dataUpdateHub)
        {
            _context = context;
            _dataUpdateHub = dataUpdateHub;
        }
            
        // GET: api/Messages
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MessageDto>>> GetMessages()
        {
            var messages = _context.Message.Include(message => message.Comments);
            List<MessageDto> messageDtos = new List<MessageDto>();
            await messages.ForEachAsync(message => messageDtos.Add(MessageDto.FromMessage(message)));
            return messageDtos;
        }

        // GET: api/Messages/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Message>> GetMessage(Guid id)
        {
            var message = await _context.Message.FindAsync(id);

            if (message == null)
            {
                return NotFound();
            }

            return message;
        }

        //PUT: api/Messages/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMessage(Guid id, Message message)
        {
            if (id != message.Id)
            {
                return BadRequest();
            }

            _context.Entry(message).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                _dataUpdateHub.Clients.All.SendAsync("messageUpdated", MessageDto.FromMessage(message));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // POST: api/Messages
        [HttpPost]
        public async Task<ActionResult<Message>> PostMessage(MessageDto messageDto)
        {
            var message = Message.FromMessageDto(messageDto);
            _context.Message.Add(message);
            await _context.SaveChangesAsync();
            _dataUpdateHub.Clients.All.SendAsync("newMessageAdded", messageDto);

            return CreatedAtAction("GetMessage", new { id = message.Id }, message);
        }

        // POST: api/Messages/updateList
        [HttpPut("updateList")]
        public async Task<IActionResult> PostMessageList(MessageDto[] messageDtos)
        {
            List<Message> messages = new List<Message>();
            foreach (var messageDto in messageDtos)
            {
                messages.Add(Message.FromMessageDto(messageDto));
            }
            foreach (var message in messages)
            {
                _context.Entry(message).State = EntityState.Modified;
            }
            try
            {
                await _context.SaveChangesAsync();
                _dataUpdateHub.Clients.All.SendAsync("messageListUpdated", messageDtos);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        // DELETE: api/Messages/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Message>> DeleteMessage(Guid id)
        {
            var message = await _context.Message.FindAsync(id);
            if (message == null)
            {
                return NotFound();
            }

            _context.Message.Remove(message);
            await _context.SaveChangesAsync();

            _dataUpdateHub.Clients.All.SendAsync("messageDeleted", id);
            return message;
        }

        private bool MessageExists(Guid id)
        {
            return _context.Message.Any(e => e.Id == id);
        }
    }
}
