﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eimantas_Kavaliauskas.Models
{
    public class MessageDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Email { get; set; }
        public List<CommentDto> Comments { get; set; }
        public string AddedOn { get; set; }
        public int Index { get; set; }


        public static MessageDto FromMessage(Message message)
        {
            List<CommentDto> commentDtos = new List<CommentDto>();
            message.Comments.ForEach(comment => commentDtos.Add(CommentDto.FromComment(comment)));
            var messageDto = new MessageDto
            {
                AddedOn = message.AddedOn,
                Email = message.Email,
                Id = message.Id,
                Text = message.Text,
                Title = message.Title,
                Comments = commentDtos,
                Index = message.Index
        };
            return messageDto;
        }
    }
}
