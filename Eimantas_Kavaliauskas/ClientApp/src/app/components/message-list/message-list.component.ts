import {Component, OnInit} from '@angular/core';
import {Message} from '../../interfaces/message';
import {ConnectorService} from '../../services/connector.service';
import {CommunicationIndicatorService} from '../../services/communication-indicator.service';
import {Guid} from '../../services/guid-generator';
import {MessageEventMediatorService} from '../../services/message-event-mediator.service';
import {ValidatorService} from '../../services/validator.service';
import {MatSnackBar} from '@angular/material';
import * as signalR from '@aspnet/signalr';
import {MessageComment} from '../../interfaces/message-comment';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit {

  hubConnection: signalR.HubConnection;
  messages: Message[] = [];
  newMessage: Message = {id: '', text: '', title: '', comments: [], index: 0, email: '', addedOn: null};
  savingInProgress = false;

  constructor(private connector: ConnectorService,
              private snackBar: MatSnackBar,
              private communicationIndicator: CommunicationIndicatorService,
              private messageEventMediator: MessageEventMediatorService) { }


  ngOnInit() {
    this.messageEventMediator.deleteMessage.subscribe(result => {
      this.deleteMessage(result);
    });
    this.communicationIndicator.savingInProgress.subscribe(savingInProgress => {
      this.savingInProgress = savingInProgress;
    });
    this.getMessageList();
    this.startHubConnection();
    this.addDataHubListener();
  }

  startHubConnection() {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:5001/dataUpdateHub')
      .build();

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  addDataHubListener () {
    this.hubConnection.on('newMessageAdded', (data) => {
      this.addMessageFromHub(data);
    });
    this.hubConnection.on('messageListUpdated', (data) => {
      this.updateMessageListFromHub(data);
    });
    this.hubConnection.on('messageDeleted', (data) => {
      this.deleteMessageFromHub(data);
    });
    this.hubConnection.on('commentAdded', (data) => {
      this.addCommentFromHub(data);
    });
    this.hubConnection.on('messageUpdated', (data) => {
      this.updateMessageFromHub(data);
    });
  }

  updateMessageFromHub(message: Message) {
    const index = this.messages.findIndex(m => m.id === message.id);
    if (index !== -1) {
      this.messages[index] = message;
    }
  }

  addMessageFromHub(message: Message) {
    if (this.messages.findIndex(m => m.id === message.id) === -1) {
      this.messages.unshift(message);
    }
  }

  updateMessageListFromHub(messagesFromHub) {
    this.checkForDeletedMessages(messagesFromHub);
    this.messages = messagesFromHub;
  }

  checkForDeletedMessages(messagesFromHub) {
    this.messages.forEach(message => {
      const index = messagesFromHub.findIndex(hubMessage => hubMessage.id === message.id);
      if (index === -1) {
        this.messageEventMediator.deleteMessageFromHub.next(message.id);
      }
    });
  }

  deleteMessageFromHub(messageId) {
    const index = this.messages.findIndex(message => message.id === messageId);
    if (index !== -1) {
      this.messageEventMediator.deleteMessageFromHub.next(messageId);
      this.messages.splice(index, 1);
    }
  }

  addCommentFromHub(comment: MessageComment) {
    const messageIndex = this.messages.findIndex(message => message.id === comment.messageId);
    const commentIndex = this.messages[messageIndex].comments.findIndex(c => c.id === comment.id);
    if (commentIndex === -1){
      this.messages[messageIndex].comments.push(comment);
    }
  }

  deleteMessage(message: Message) {
    const index = this.messages.findIndex(m => m.id === message.id);
    this.messages.splice(index, 1);
    this.recountIndexes();
    this.communicationIndicator.increaseSaveQueue();
    this.connector.SaveMessageList(this.messages).subscribe(result => {
      this.communicationIndicator.decreaseSaveQueue();
    });
  }

  getMessageList() {
    this.communicationIndicator.increaseSaveQueue();
    this.connector.GetMessages().subscribe(result => {
      this.communicationIndicator.decreaseSaveQueue();
      this.messages = result;
      this.messages.forEach(message => {
        if (message.comments === null) {
          message.comments = [];
        }
      });
      this.sortList();
      this.newMessage.index = this.messages.length;
    }, error => {
      this.communicationIndicator.decreaseSaveQueue();
    });
  }

  onPostClick() {
    if (!this.validate()) {
      return;
    } else {
      const temporaryGuid = Guid.newGuid();
      this.newMessage.addedOn = new Date();
      this.newMessage.id = temporaryGuid;
      this.newMessage.index = this.messages.length + 1;
      this.messages.unshift(this.newMessage);
      this.recountIndexes();
      this.communicationIndicator.increaseSaveQueue();
      this.connector.PostMessage(this.newMessage).subscribe(result => {
        this.communicationIndicator.decreaseSaveQueue();
        const index = this.messages.findIndex(message => message.id === temporaryGuid);
        this.messages[index].id = result.id;
      });
      this.newMessage = {id: '', text: '', title: '', comments: [], index: this.messages.length, email: '', addedOn: null};
    }
  }

  validate(): boolean {
    if (this.newMessage.title === '' || this.newMessage.email === '' || this.newMessage.text === '') {
      this.showError('Please fill in all the fields', '');
      return false;
    }
    if (ValidatorService.validateEmail(this.newMessage.email)) {
      return true;
    }
    this.showError('You have entered an invalid email address!', '');
    return false;
  }

  showError(message, action) {
    this.snackBar.open(message, action, {
      duration: 1000,
    });
  }

  sortList() {
    const sortingFunction = (a: Message, b: Message) => {
      if (a.index > b.index ) { return -1; }
      if (a.index < b.index ) { return 1; }
      if (a.index === b.index ) { return 0; }
    };
    this.messages.sort(sortingFunction);
  }

  recountIndexes() {
    let counter = this.messages.length;
    this.messages.forEach(message => {
      message.index = counter;
      counter--;
    });
  }
}
