import {Component, Input, OnInit} from '@angular/core';
import {Message} from '../../../interfaces/message';
import {MatDialog} from '@angular/material';
import {MessageEditDialogComponent} from './message-edit-dialog/message-edit-dialog.component';
import {ConnectorService} from '../../../services/connector.service';
import {CommunicationIndicatorService} from '../../../services/communication-indicator.service';
import {MessageEventMediatorService} from '../../../services/message-event-mediator.service';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss']
})
export class MessageItemComponent implements OnInit {
  @Input() message: Message;

  constructor(public dialog: MatDialog,
              private connector: ConnectorService,
              private communicationIndicator: CommunicationIndicatorService,
              private messageEventMediator: MessageEventMediatorService) { }

  ngOnInit() {
  }

  deleteMessage(event) {
    event.stopPropagation();
    this.messageEventMediator.deleteMessage.next(this.message);
    this.communicationIndicator.increaseSaveQueue();
    this.connector.DeleteMessage(this.message).subscribe(result => {
      this.communicationIndicator.decreaseSaveQueue();
    });
  }

  onClick() {
    const messageClone = {...this.message};
    const dialogRef = this.dialog.open(MessageEditDialogComponent, {
      data: { message: this.message }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        this.message = messageClone;
      }
    });
  }
}
