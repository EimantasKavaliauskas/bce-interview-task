import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Message} from '../interfaces/message';

@Injectable({
  providedIn: 'root'
})
export class MessageEventMediatorService {

  private _deleteMessage = new Subject<Message>();
  private _deleteMessageFromHub = new Subject<string>();

  get deleteMessage() {
    return this._deleteMessage;
  }

  get deleteMessageFromHub() {
    return this._deleteMessageFromHub;
  }

  constructor() { }
}
