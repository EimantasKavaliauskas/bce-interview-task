import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommunicationIndicatorService {

  saveQueue = 0;
  savingInProgress = new Subject<boolean>();


  constructor() { }

  increaseSaveQueue() {
    this.savingInProgress.next(true);
    this.saveQueue++;
  }

  decreaseSaveQueue() {
    this.saveQueue--;
    if (this.saveQueue === 0) {
      this.savingInProgress.next(false);
    }
  }
}
